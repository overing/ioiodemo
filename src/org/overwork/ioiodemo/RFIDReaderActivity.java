package org.overwork.ioiodemo;

import ioio.lib.api.DigitalInput;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.IOIOFactory;
import ioio.lib.api.Uart;
import ioio.lib.api.exception.ConnectionLostException;

import java.io.InputStream;
import java.math.BigInteger;
import java.util.Arrays;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ToggleButton;

public class RFIDReaderActivity extends Activity {

    private static final String TAG = RFIDReaderActivity.class.getSimpleName();

    private IOIOThread mIOIOThread;

    private ToggleButton mToggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rfid_reader);

        mToggleButton = (ToggleButton) findViewById(R.id.toggleButton_Stat);

        // TODO Auto-generated method stub
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIOIOThread = new IOIOThread();
        mIOIOThread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIOIOThread.abort();
        try {
            mIOIOThread.join();
        } catch (InterruptedException e) {
        }
    }

    private class IOIOThread extends Thread {

        private IOIO mIOIO;
        private boolean mAbort = false;

        @Override
        public void run() {
            while (true) {
                synchronized (this) {
                    if (mAbort) {
                        break;
                    }
                    mIOIO = IOIOFactory.create();
                }
                try {
                    setText("Waiting for IOIO to connect...");
                    mIOIO.waitForConnect();
                    setText("IOIO connected!");

                    DigitalOutput stat = mIOIO.openDigitalOutput(0, true);
                    //
                    DigitalInput.Spec rx = new DigitalInput.Spec(1, DigitalInput.Spec.Mode.PULL_DOWN);
                    DigitalOutput.Spec tx = new DigitalOutput.Spec(2, DigitalOutput.Spec.Mode.OPEN_DRAIN);
                    Uart reader = mIOIO.openUart(rx, tx, 57600, Uart.Parity.NONE, Uart.StopBits.ONE);
                    InputStream input = reader.getInputStream();
//                    OutputStream output = reader.getOutputStream();

                    byte[] buffer = new byte[1024], content;

                    while (true) {
                        stat.write(!mToggleButton.isChecked());
                        //
                        if (input.available() > 0) {
                            int read = input.read(buffer);
                            content = Arrays.copyOf(buffer, read);
                            Log.w(TAG, "read=" + //
                                    new BigInteger(1, content).toString(16));
                        }

                        sleep(20);
                    }
                } catch (ConnectionLostException e) {
                } catch (Exception e) {
                    Log.e(TAG, "Unexpected exception caught", e);
                    mIOIO.disconnect();
                    break;
                } finally {
                    if (mIOIO != null) {
                        try {
                            mIOIO.waitForDisconnect();
                        } catch (InterruptedException e) {
                        }
                    }
                    synchronized (this) {
                        mIOIO = null;
                    }
                }
            }
        }

        synchronized public void abort() {
            mAbort = true;
            if (mIOIO != null) {
                mIOIO.disconnect();
            }
        }

        private void setText(final String text) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTitle(text);
                }
            });
        }
    }
}
