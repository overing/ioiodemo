package org.overwork.ioiodemo;

import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.content.Intent;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.button_Joystick:
            startActivity(new Intent(this, JoystickActivity.class));
            break;

        case R.id.button_RGB_LED:
            startActivity(new Intent(this, RGBLEDActivity.class));
            break;

        case R.id.button_Breathalyzer:
            startActivity(new Intent(this, BreathalyzerActivity.class));
            break;

        case R.id.button_RFIDReader:
            startActivity(new Intent(this, RFIDReaderActivity.class));
            break;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button_Joystick).setOnClickListener(this);
        findViewById(R.id.button_RGB_LED).setOnClickListener(this);
        findViewById(R.id.button_Breathalyzer).setOnClickListener(this);
        findViewById(R.id.button_RFIDReader).setOnClickListener(this);
    }
}
