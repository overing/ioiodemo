package org.overwork.ioiodemo;

import ioio.lib.api.AnalogInput;
import ioio.lib.api.IOIO;
import ioio.lib.api.IOIOFactory;
import ioio.lib.api.exception.ConnectionLostException;
import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class JoystickActivity extends Activity //
        implements SurfaceHolder.Callback {

    private static final String TAG = JoystickActivity.class.getSimpleName();

    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;

    private IOIOThread mIOIOThread;

    private Paint mPaint;

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mIOIOThread = new IOIOThread();
        mIOIOThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mIOIOThread.abort();
        try {
            mIOIOThread.join();
        } catch (InterruptedException e) {
        }
        mIOIOThread = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joystick);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(28f);
        mPaint.setColor(Color.BLACK);
        mPaint.setStrokeWidth(2.6f);

        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
    }

    private void draw(Canvas c, float x, float y) {
        float cx = c.getWidth() / 2;
        float cy = c.getHeight() / 2;
        float radius = c.getWidth() / 2f * 0.8f;

        c.drawColor(Color.WHITE);

        mPaint.setStyle(Paint.Style.STROKE);
        c.drawCircle(cx, cy, radius, mPaint);

        float dx = cx + (x - 0.5f) * 2 * radius;
        float dy = cy + (0.5f - y) * 2 * radius;
        c.drawLine(cx, cy, dx, dy, mPaint);

        mPaint.setStyle(Paint.Style.FILL);
        c.drawText(String.format("X=%04.3f, Y=%04.3f", x, y), //
                cx, c.getHeight() - mPaint.getTextSize(), mPaint);
    }

    private class IOIOThread extends Thread {

        private IOIO mIOIO;
        private boolean mAbort = false;

        private AnalogInput mAnalogInput_X;
        private AnalogInput mAnalogInput_Y;

        @Override
        public void run() {
            while (true) {
                synchronized (this) {
                    if (mAbort) {
                        break;
                    }
                    mIOIO = IOIOFactory.create();
                }
                try {
                    setText("Waiting for IOIO to connect...");
                    mIOIO.waitForConnect();
                    setText("IOIO connected!");

                    mIOIO.openDigitalOutput(0, false);

                    mIOIO.openDigitalOutput(1, true);
                    mIOIO.openDigitalOutput(46, true);

                    mAnalogInput_X = mIOIO.openAnalogInput(34);
                    mAnalogInput_Y = mIOIO.openAnalogInput(36);

                    while (true) {
                        float x = mAnalogInput_X.read();
                        float y = mAnalogInput_Y.read();
                        Canvas c = null;
                        try {
                            c = mSurfaceHolder.lockCanvas();
                            draw(c, x, y);
                        } finally {
                            mSurfaceHolder.unlockCanvasAndPost(c);
                        }

                        sleep(20);
                    }
                } catch (ConnectionLostException e) {
                } catch (Exception e) {
                    Log.e(TAG, "Unexpected exception caught", e);
                    mIOIO.disconnect();
                    break;
                } finally {
                    if (mIOIO != null) {
                        try {
                            mIOIO.waitForDisconnect();
                        } catch (InterruptedException e) {
                        }
                    }
                    synchronized (this) {
                        mIOIO = null;
                    }
                }
            }
        }

        synchronized public void abort() {
            mAbort = true;
            if (mIOIO != null) {
                mIOIO.disconnect();
            }
        }

        private void setText(final String text) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTitle(text);
                }
            });
        }
    }
}
