package org.overwork.ioiodemo;

import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.IOIOFactory;
import ioio.lib.api.exception.ConnectionLostException;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.ToggleButton;

public class RGBLEDActivity extends Activity {

    private static final String TAG = RGBLEDActivity.class.getSimpleName();

    private ToggleButton mToggle_Stat;
    private ToggleButton mToggle_RGB_LED;
    private SeekBar mSeekBar_Red;
    private SeekBar mSeekBar_Green;
    private SeekBar mSeekBar_Blue;
    private Spinner mSpinner_Transits;

    private IOIOThread mIOIOThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rgb_led);

        mToggle_Stat = (ToggleButton) findViewById(R.id.toggleButton_Stat);
        mToggle_RGB_LED = (ToggleButton) findViewById(R.id.toggleButton_RGB_LED);

        mSeekBar_Red = (SeekBar) findViewById(R.id.seekBar_Red);
        mSeekBar_Green = (SeekBar) findViewById(R.id.seekBar_Green);
        mSeekBar_Blue = (SeekBar) findViewById(R.id.seekBar_Blue);

        mSpinner_Transits = (Spinner) findViewById(R.id.spinner_Transits);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIOIOThread = new IOIOThread();
        mIOIOThread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIOIOThread.abort();
        try {
            mIOIOThread.join();
        } catch (InterruptedException e) {
        }
    }

    private class IOIOThread extends Thread {

        private IOIO mIOIO;
        private boolean mAbort = false;

        private DigitalOutput mDigitalOutput_Stat;
        private DigitalOutput mDigitalOutput_LED_SW;
        private RGBLEDDriver mLEDriver;

        @Override
        public void run() {
            while (true) {
                synchronized (this) {
                    if (mAbort) {
                        break;
                    }
                    mIOIO = IOIOFactory.create();
                }
                try {
                    setText("Waiting for IOIO to connect...");
                    mIOIO.waitForConnect();
                    setText("IOIO connected!");

                    mDigitalOutput_Stat = mIOIO.openDigitalOutput(0, true);

                    mDigitalOutput_LED_SW = mIOIO.openDigitalOutput(1);
                    mLEDriver = new RGBLEDDriver(mIOIO.openDigitalOutput(3),
                            mIOIO.openDigitalOutput(2));

                    while (true) {
                        mDigitalOutput_Stat.write(!mToggle_Stat.isChecked());

                        mDigitalOutput_LED_SW
                                .write(mToggle_RGB_LED.isChecked());

                        byte red = (byte) mSeekBar_Red.getProgress();
                        byte green = (byte) mSeekBar_Green.getProgress();
                        byte blue = (byte) mSeekBar_Blue.getProgress();
                        int transits = Integer.parseInt(mSpinner_Transits
                                .getSelectedItem().toString());
                        mLEDriver.begin();
                        for (int i = 0; i < transits; i++) {
                            mLEDriver.setColor(red, green, blue);
                        }
                        mLEDriver.end();

                        sleep(1000);
                    }
                } catch (ConnectionLostException e) {
                } catch (Exception e) {
                    Log.e(TAG, "Unexpected exception caught", e);
                    mIOIO.disconnect();
                    break;
                } finally {
                    if (mIOIO != null) {
                        try {
                            mIOIO.waitForDisconnect();
                        } catch (InterruptedException e) {
                        }
                    }
                    synchronized (this) {
                        mIOIO = null;
                    }
                }
            }
        }

        synchronized public void abort() {
            mAbort = true;
            if (mIOIO != null) {
                mIOIO.disconnect();
            }
        }

        private void setText(final String text) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTitle(text);
                }
            });
        }
    }

    public static class RGBLEDDriver {

        private static final boolean HIGH = true;
        private static final boolean LOW = !HIGH;
        private final DigitalOutput mClockPin, mDataPin;

        public RGBLEDDriver(DigitalOutput clockPin, DigitalOutput dataPin) {
            mClockPin = clockPin;
            mDataPin = dataPin;
        }

        public void begin() throws ConnectionLostException {
            send32Zero();
        }

        public void end() throws ConnectionLostException {
            send32Zero();
        }

        /**
         * 
         * @param red
         *            value: 0 ~ 127
         * @param green
         *            value: 0 ~ 127
         * @param blue
         *            value: 0 ~ 127
         * @throws ConnectionLostException
         */
        public void setColor(byte red, byte green, byte blue)
                throws ConnectionLostException {
            int dx = 0x00;

            dx |= 0x03 << 30;
            dx |= takeAntiCode(blue) << 28;
            dx |= takeAntiCode(green) << 26;
            dx |= takeAntiCode(red) << 24;

            dx |= blue << 16;
            dx |= green << 8;
            dx |= red;

            sendData(dx);
        }

        private void send32Zero() throws ConnectionLostException {
            for (int i = 0; i < 32; i++) {
                mDataPin.write(LOW);
                clockRise();
            }
        }

        private void sendData(int dx) throws ConnectionLostException {
            for (int i = 0; i < 32; i++) {
                if ((dx & 0x80000000) != 0) {
                    mDataPin.write(HIGH);
                } else {
                    mDataPin.write(LOW);
                }

                dx <<= 1;
                clockRise();
            }
        }

        private void clockRise() throws ConnectionLostException {
            mClockPin.write(LOW);
            try {
                Thread.sleep(0, 1000 * 20);
            } catch (InterruptedException ex) {
            }

            mClockPin.write(HIGH);
            try {
                Thread.sleep(0, 1000 * 20);
            } catch (InterruptedException ex) {
            }
        }

        private static byte takeAntiCode(byte data) {
            byte temp = 0;

            if ((data & 0x80) == 0) {
                temp |= 0x02;
            }

            if ((data & 0x40) == 0) {
                temp |= 0x01;
            }

            return temp;
        }
    }
}
