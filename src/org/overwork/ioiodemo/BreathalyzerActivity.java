package org.overwork.ioiodemo;

import ioio.lib.api.AnalogInput;
import ioio.lib.api.DigitalOutput;
import ioio.lib.api.IOIO;
import ioio.lib.api.IOIOFactory;
import ioio.lib.api.exception.ConnectionLostException;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

public class BreathalyzerActivity extends Activity {

    private static final String TAG = "BreathalyzerActivity";

    private IOIOThread mIOIOThread;

    private ToggleButton mToggleButton;
    private ProgressBar mProgressBar;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breathalyzer);

        mToggleButton = (ToggleButton) findViewById(R.id.toggleButton_Stat);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setMax(1000);
        
        mTextView = (TextView) findViewById(R.id.textView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIOIOThread = new IOIOThread();
        mIOIOThread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIOIOThread.abort();
        try {
            mIOIOThread.join();
        } catch (InterruptedException e) {
        }
    }

    private class IOIOThread extends Thread {

        private IOIO mIOIO;
        private boolean mAbort = false;

        @Override
        public void run() {
            while (true) {
                synchronized (this) {
                    if (mAbort) {
                        break;
                    }
                    mIOIO = IOIOFactory.create();
                }
                try {
                    setText("Waiting for IOIO to connect...");
                    mIOIO.waitForConnect();
                    setText("IOIO connected!");

                    DigitalOutput stat = mIOIO.openDigitalOutput(0, true);
                    //
                    AnalogInput alcoholInput = mIOIO.openAnalogInput(40);

                    while (true) {
                        stat.write(!mToggleButton.isChecked());
                        //
                        final float alcohol = alcoholInput.read();
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                int progress = (int) (alcohol * 1000);
                                mProgressBar.setProgress(progress);
                                
                                mTextView.setText(String.format("Alcohol=%06.3f", alcohol));
                            }
                        });

                        sleep(20);
                    }
                } catch (ConnectionLostException e) {
                } catch (Exception e) {
                    Log.e(TAG, "Unexpected exception caught", e);
                    mIOIO.disconnect();
                    break;
                } finally {
                    if (mIOIO != null) {
                        try {
                            mIOIO.waitForDisconnect();
                        } catch (InterruptedException e) {
                        }
                    }
                    synchronized (this) {
                        mIOIO = null;
                    }
                }
            }
        }

        synchronized public void abort() {
            mAbort = true;
            if (mIOIO != null) {
                mIOIO.disconnect();
            }
        }

        private void setText(final String text) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTitle(text);
                }
            });
        }
    }
}
