package org.overwork.ioiodemo;

import ioio.lib.util.IOIOConnectionRegistry;
import android.app.Application;

public class IOIODemoApp extends Application {

    static {
        IOIOConnectionRegistry.addBootstraps(new String[] {//
                "ioio.lib.android.bluetooth.BluetoothIOIOConnectionBootstrap" });
    }
}
